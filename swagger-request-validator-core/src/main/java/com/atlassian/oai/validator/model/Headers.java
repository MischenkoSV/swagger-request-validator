package com.atlassian.oai.validator.model;

public interface Headers {

    String CONTENT_TYPE = "Content-Type";
    String ACCEPT = "Accept";

}
